###
# Copyright (c) 2009, Joseph Walton-Rivers
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from xml.dom.minidom import parse, parseString, Document
import urllib2
from urllib import urlencode
from string import join

class Trueknowledge(callbacks.Plugin):
    """Add the help for "@plugin help Trueknowledge" here
    This should describe *how* to use this plugin."""
    pass

    def ask(self, irc, msg, args, question):
        """<question>

        Returns the calculated answer from trueKnowledge (http://www.trueknowledge.com/)
        """
        api_username = ""
        password = ""
        question = join(question)

        #get the dom object
	dom = parseString(urllib2.urlopen('https://api.trueknowledge.com/direct_answer?api_account_id='+api_username+'&api_password='+password+'&question='+question+'&structured_response=0').read().replace('\n',''))
	#give the user the factoid
	irc.reply(dom.getElementsByTagName('tk:text_result')[0].firstChild.nodeValue)
    irc.reply('cite: http://www.trueknowledge.com/')
	dom.unlink()

    ask = wrap(ask, [many('anything')])

Class = Trueknowledge


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
