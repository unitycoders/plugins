###
# WitchHunt - a supybot game plugin
# Copyright (C) 2009  Unity Coders (http://www.unitycoders.co.uk)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks


class WitchHunt(callbacks.Plugin):
	"""DEVELOPMENTAL - THIS PLUGIN WILL NOT FUNCTION!"""
	stage = 0
	players = []
	witches = []
	

	def start(self, irc, msg, args):
		"""takes no arguments

		Starts a new game of WitchHunt
		"""
		if self.stage == 0:
			irc.reply('A new game has started. Please use the join command if you wish to play.')
			self.stage = 1 #A game has started, waiting for players
			self.players = [] #Make the players list empty
		else:
			irc.error('A game is in progress already.')
	start = wrap(start)

	def join(self, irc, msg, args):
		"""takes no arguments

		Joins the active game
		"""
		if self.stage == 1:
			irc.reply(msg.nick+' has joined the current game')
			self.players.append(msg.nick)
		else:
			irc.error('The game is not currently accepting new players')
	join = wrap(join)

	def begin(self, irc, msg, args):
		"""takes no arguments

		Begins a game (providing there are enough players)
		"""
		if self.stage == 1:
			irc.reply('The game has now started. No new players can join. Please check your PrivMesgs for instructions. If you do not have a PM from me you are not a witch.')
			self.stage = 2 # begin night time
		else:
			irc.error('The game cannot be started.')
	begin = wrap(begin)

	def burn(self, irc, msg, args, user):
		"""takes no arguments

		Vote for a player to burn
		"""
		pass
	burn = wrap(burn, ('anything'))

Class = WitchHunt
