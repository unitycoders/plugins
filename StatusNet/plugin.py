###
# Copyright (c) 2010, Joseph Walton-Rivers
# All rights reserved.
#
#
###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from xml.dom.minidom import parse
from urllib2 import urlopen

class StatusNet(callbacks.Plugin):
    """Add the help for "@plugin help StatusNet" here
    This should describe *how* to use this plugin."""

    def status(self, irc, msg, args, statusID):
	"""StatusID
	
	Returns the status speified by statusID
	"""
	#Get the status and the author
        result = parse(urlopen("http://identi.ca/api/statuses/show/"+str(statusID)+".xml"))
        status = result.getElementsByTagName("text")[0].childNodes[0].nodeValue
        name = result.getElementsByTagName("screen_name")[0].childNodes[0].nodeValue
        result.unlink()
        irc.reply("Dent #"+str(statusID)+" "+name+" : "+status)
    status = wrap(status, ['int'])

    def group(self, irc, msg, args, groupID):
        """GroupID

        Returns the Group's last statuses"""
        result = parse(urlopen("http://identi.ca/api/statusnet/groups/timeline/"+str(groupID)+".xml"))
	for statusNo in range(0, result.getElementsByTagName("statuses").length):
            status = result.getElementsByTagName("statuses")[statusNo]
            dent = status.getElementsByTagName("text")[0].childNodes[0].nodeValue
            name = status.getElementsByTagName("screen_name")[0].childNodes[0].nodeValue
            irc.reply(name+" : "+dent)
    group = wrap(group, ['int'])
Class = StatusNet

# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
