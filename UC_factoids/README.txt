Communicates with Webpigeon's CakePHP module, caching the results of each factoid. This is useful as it allows our factoid database to be viewed online without too much trouble.
