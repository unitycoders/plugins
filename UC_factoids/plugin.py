###
# Copyright (c) 2009, Joseph Walton-Rivers
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import cakeConnect
import urllib2
from string import join

class UC_factoids(callbacks.Plugin):
    """Add the help for "@plugin help UC_factoids" here
    This should describe *how* to use this plugin."""
    conn = cakeConnect.cakeConnect('http://localhost/cakephp/unitycoders')

    def remember(self, irc, msg, args, factstr, factbody):
        """takes no arguments

        Returns the next random number from the random number generator.
        """
        try:
            Dict1                       = dict()
            Dict1['factoid']            = dict()
            Dict1['factoid']['title']   = factstr
            Dict1['factoid']['body']    = join(factbody, ' ')
            self.conn.add('factoids', Dict1)
            irc.reply("I now know about "+factstr)
        except:
            irc.error("Sorry, I can't remember that")
    remember = wrap(remember, ['anything', many('anything')])

    def factoid(self, irc, msg, args, factstr):
        """<factoid>

        Returns the fact assoicated with the provided factoid from the factoid database
        """
        try:
            dom = self.conn.view('factoids', factstr)
	        #give the user the factoid
            irc.reply(dom.getElementsByTagName('factoid')[0].attributes["body"].value)
            dom.unlink()
    	except:
    	    irc.error("Sorry, I don't know anything about "+factstr)
    factoid = wrap(factoid, ['anything'])

    def replace(self, irc, msg, args, factstr, factbody):
        """takes no arguments

        Returns the next random number from the random number generator.
        """
        try:
            Dict1                       = dict()
            Dict1['factoid']            = dict()
            Dict1['factoid']['body']    = join(factbody, ' ')
            self.conn.edit('factoids', factstr, Dict1)
            irc.replySuccess()
        except:
            irc.error("Sorry, I can't remember that")
    replace = wrap(replace, ['anything', many('anything')])

    def forget(self, irc, msg, args, factstr):
        """takes no arguments

        Returns the next random number from the random number generator.
        """
        try:
            self.conn.remove('factoids', factstr)
            irc.replySuccess()
        except:
            irc.error("Sorry, I can't forget that")
    forget = wrap(forget, ['anything'])

Class = UC_factoids


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
