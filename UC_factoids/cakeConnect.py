#! /usr/bin/python
from xml.dom.minidom import parse, parseString, Document
import urllib2

class cakeConnect:
	httpObject = None
	url = None

	def __init__(self, url):
		self.url = url

	def toXml(self, objectType, data):
		if data == None:
			return
		Doc = Document()
		root = Doc.createElement(objectType)
		for objectItem in data.iteritems():
			elm = root.createElement(objectItem[0])
			for objectAttr in objectItem[1].iteritems():
				elm.setAttribute(objectAttr[0], objectAttr[1])
			Doc.appendChild(elm)
		return Doc.toxml()

	def run(self, url, methord, objectType=None, data=None):
		req = urllib2.Request(url, self.toXml(objectType, data))
		req.add_header('content-type', 'application/xml')
		req.add_header('REQUEST_METHOD', methord)
		result = urllib2.urlopen(req)
		return parseString(result.read().replace('\n',''))

	def index(self, objectType):
		url = self.url+'/'+objectType+'/index.xml'
		return self.run(url, 'GET')

	def view(self, objectType, name):
		url = self.url+'/'+objectType+'/view/'+name+'.xml'
		return self.run(url, 'GET')

	def add(self, objectType, data):
		url = self.url+'/'+objectType+'/add.xml'
		return self.run(url, 'POST', objectType, data)

	def edit(self, objectType, name, data):
		url = self.url+'/'+objectType+'/edit/'+name+'.xml'
		return self.run(url, 'PUT', objectType, data)

	def remove(self, objectType, name):
		url = self.url+'/'+objectType+'/remove/'+name+'.xml'
		return self.run(url, 'DELETE')
