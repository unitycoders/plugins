###
# Copyright (c) 2009, Joseph Walton-Rivers
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks

#Time imports
from datetime import datetime
from pytz import timezone, UnknownTimeZoneError

class Worldclock(callbacks.Plugin):
	"""Add the help for "@plugin help Worldclock" here
	This should describe *how* to use this plugin."""

	def localtime(self, irc, msg, args):
		"""Takes no arguments.
		
		Displays the time in England (the location of the bot)"""
		irc.reply(datetime.now().strftime('%A %d %B %Y %H:%M'))
	localtime = wrap(localtime)    

	def worldtime(self, irc, msg, args, channel, timezonestr, format):
		"""<channel> <timezone> <format>

           	 Displays time in spesificed timezone."""
		try:
			tz = timezone(timezonestr)
			if not format:
				format = '%A %d %B %Y %H:%M' #TODO Config goes here :)
			irc.reply(datetime.now(tz).strftime(format))
		except UnknownTimeZoneError:
			irc.error('I don\'t know about '+timezonestr)
	worldtime = wrap(worldtime, [optional('channel'), 'anything', optional('nonInt')])

Class = Worldclock

# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
